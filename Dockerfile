FROM openjdk:8-jdk-alpine
MAINTAINER Jay Chang <jaychang1987@gmail.com>
VOLUME /tmp
ARG JAR_FILE
ARG SERVER_PORT
ADD ${JAR_FILE} app.jar
# Configure ustc alipine software source and timezone
RUN echo -e "https://mirrors.ustc.edu.cn/alpine/latest-stable/main\nhttps://mirrors.ustc.edu.cn/alpine/latest-stable/community" > /etc/apk/repositories && \
    apk update && \
    apk --no-cache add tzdata && \
    cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" >  /etc/timezone
EXPOSE ${SERVER_PORT}
# To reduce Tomcat startup time we added a system property pointing to "/dev/urandom" as a source of entropy.
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]